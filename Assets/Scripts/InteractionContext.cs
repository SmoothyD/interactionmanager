using UnityEngine;

public class InteractionContext
{
    // Element selected when pressing down
    public InteractiveElement FirstInteractiveElement { get; set; }

    // Current element, in case moving the cursor while holding it down
    public InteractiveElement CurrentInteractiveElement { get; set; }
    
    public InteractionHandlers BlockingHandlers { get; set; }

    public ActionType ActionType { get; set; }
    public float TimeAtPress { get; set; }
    public InteractionInputData InputData { get; set; }
}

public class InteractionInputData
{
    public Vector2 StartingPosition { get; set; }
    public Touch Touch { get; set; }
}