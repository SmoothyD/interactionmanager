using System;
using System.Collections.Generic;

public class InteractionHandlers
{
    public string Tag { get; }
    public Func<InteractionContext, bool> PressHandler { get; }
    public Func<InteractionContext, bool> HoldHandler { get; }
    public Action<InteractionContext> ChangedHandler { get; }
    public Func<InteractionContext, bool> ReleaseHandler { get; }
    public Action<InteractionContext> ClickHandler { get; }

    public InteractionHandlers(string tag = null,
                               Func<InteractionContext, bool> pressHandler = null,
                               Func<InteractionContext, bool> holdHandler = null,
                               Action<InteractionContext> changedHandler = null,
                               Func<InteractionContext, bool> releaseHandler = null,
                               Action<InteractionContext> clickHandler = null)
    {
        PressHandler = pressHandler;
        HoldHandler = holdHandler;
        ReleaseHandler = releaseHandler;
        ClickHandler = clickHandler;
        ChangedHandler = changedHandler;
        Tag = tag;
    }
}