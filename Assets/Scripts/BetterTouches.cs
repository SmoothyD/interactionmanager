using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BetterTouches : UnitySingleton<BetterTouches>
{
    public Dictionary<int, Touch> TouchesByFingerId { get; set; } = new Dictionary<int, Touch>();
    public List<Touch> NewTouches { get; set; } = new List<Touch>();
    public int MouseFingerId = 11;

    private readonly WaitForEndOfFrame _waitForEndOfFrame = new WaitForEndOfFrame();
    private Touch _mouseTouch;
    private Vector3 _previousMousePosition;

    private void Start()
    {
        _mouseTouch.phase = TouchPhase.Canceled;
    }

    private void Update()
    {
        TouchesByFingerId = Input.touches.ToDictionary(touch => touch.fingerId, touch => touch);
        NewTouches = Input.touches.Where(touch => touch.phase == TouchPhase.Began).ToList();

        if (Input.GetMouseButtonDown(0) && !Input.touches.Any())
        {
            _mouseTouch = new Touch
            {
                phase = TouchPhase.Began,
                position = Input.mousePosition,
                pressure = 1,
                type = TouchType.Direct,
                deltaPosition = Vector3.zero,
                deltaTime = 0,
                fingerId = MouseFingerId,
            };

            _previousMousePosition = Input.mousePosition;
            NewTouches.Add(_mouseTouch);

            StartCoroutine(UpdateMouseTouch());
        }
        if (_mouseTouch.phase != TouchPhase.Canceled)
        {
            TouchesByFingerId[MouseFingerId] = _mouseTouch;
        }
    }

    private IEnumerator UpdateMouseTouch()
    {
        yield return _waitForEndOfFrame;
        var timeElapsed = 0f;

        NewTouches.Remove(_mouseTouch);
        while (Input.GetMouseButton(0))
        {
            timeElapsed += Time.deltaTime;
            if (Input.mousePosition != _previousMousePosition)
            {
                _mouseTouch.deltaPosition = Input.mousePosition - _previousMousePosition;
                _mouseTouch.position = Input.mousePosition;
                _mouseTouch.phase = TouchPhase.Moved;
                _mouseTouch.deltaTime = timeElapsed;
                timeElapsed = 0;
            }
            else
            {
                _mouseTouch.phase = TouchPhase.Stationary;
            }
            TouchesByFingerId[MouseFingerId] = _mouseTouch;

            _previousMousePosition = Input.mousePosition;
            yield return _waitForEndOfFrame;
        }
        _mouseTouch.phase = TouchPhase.Ended;
        TouchesByFingerId[MouseFingerId] = _mouseTouch;

        yield return _waitForEndOfFrame;
        _mouseTouch.phase = TouchPhase.Canceled;
        TouchesByFingerId.Remove(_mouseTouch.fingerId);
    }
}