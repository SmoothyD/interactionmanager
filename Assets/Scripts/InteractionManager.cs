using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public enum ActionType : byte
{
    Down,
    Held,
    Up,
}

public enum CallbackReturn : byte
{
    Continue, // Next handler by order of priority will be called
    Stop,     // Next handlers won't be called for this frame
    Lock,     // Only those handlers will be called until ActionType is Up
}

public class InteractionManager : UnitySingleton<InteractionManager>
{
    public Action ReleaseCleanup { get; set; }

    private readonly SortedDictionary<int, InteractionHandlers> _interactionHandlers = new SortedDictionary<int, InteractionHandlers>();
    private LayerMask _clickableLayer;
    private LayerMask _uiLayer;
    private readonly Dictionary<int, InteractionContext> _contextsByFingerId = new Dictionary<int, InteractionContext>();

    public void AddInteractionHandlers(int priority, InteractionHandlers interactionHandlers)
    {
        _interactionHandlers[priority] = interactionHandlers;
    }

    private void Start()
    {
        _clickableLayer |= 1 << LayerMask.NameToLayer("ClickableEntity");
        _uiLayer = LayerMask.NameToLayer("InteractibleUI");
        SceneManager.sceneUnloaded += OnSceneUnloaded;
    }

    private void OnSceneUnloaded(Scene scene)
    {
        _interactionHandlers.Clear();
        ReleaseCleanup = default;
    }

    private void Update()
    {
        foreach (var touch in BetterTouches.I.TouchesByFingerId.Values)
        {
            // Checking if cursor isn't over some UI Element
            var cursor = new PointerEventData(EventSystem.current) { position = touch.position };
            var objectsHit = new List<RaycastResult>();
            EventSystem.current.RaycastAll(cursor, objectsHit);
            if (!_contextsByFingerId.ContainsKey(touch.fingerId)
             && objectsHit.Count(result => result.gameObject.layer == _uiLayer) > 0)
            {
                continue;
            }

            // UI is not blocking, so we can check for interactions with world elements
            var context = ComputeInteractionContext(touch);
            if (context == default) continue;

            var hits = Physics2D.RaycastAll(Camera.main.ScreenToWorldPoint(context.InputData.Touch.position), Vector2.zero, 100000, _clickableLayer);
            var hitElements = hits.Select(hit => hit.collider.gameObject.GetComponentInParent<InteractiveElement>()?.gameObject.GetComponentInChildren<SpriteRenderer>())
                                  .Distinct()
                                  .Where(spriteRenderer => spriteRenderer
                                                        && IsCursorOnSprite(spriteRenderer, context.InputData.Touch.position))
                                  .OrderByDescending(spriteRenderer => spriteRenderer.sortingOrder)
                                  .ThenByDescending(spriteRenderer => spriteRenderer.transform.position.z)
                                  .Select(spriteRenderer => spriteRenderer.GetComponentInParent<InteractiveElement>().gameObject)
                                  .ToList();

            if (context.BlockingHandlers == default)
            {
                InvokeAllHandlers(hitElements, context);
            }
            else
            {
                InvokeBlockingHandlers(hitElements, context);
            }
        }
    }

    /// <summary>
    /// Creates / Updates context based on click and action types
    /// </summary>
    /// <param name="touch"></param>
    /// <returns></returns>
    private InteractionContext ComputeInteractionContext(Touch touch)
    {
        // Create new contexts
        if (touch.phase == TouchPhase.Began)
        {
            var newContext = new InteractionContext
            {
                TimeAtPress = Time.time,
                InputData = new InteractionInputData
                {
                    StartingPosition = touch.position,
                    Touch = touch
                }
            };

            _contextsByFingerId[touch.fingerId] = newContext;
        }

        // Can happen after interaction with the UI. Interactions with entities shouldn't happen in this case.
        if (!_contextsByFingerId.TryGetValue(touch.fingerId, out var context)) return default;

        // Update existing contexts
        context.ActionType = GetActionType(touch.fingerId);
        context.InputData.Touch = BetterTouches.I.TouchesByFingerId[touch.fingerId];

        return context;
    }

    /// <summary>
    /// Goes through all the registered handlers by order of priority until one returns stop or lock
    /// </summary>
    /// <param name="hits"></param>
    /// <param name="context"></param>
    private void InvokeAllHandlers(List<GameObject> hits, InteractionContext context)
    {
        foreach (var interactionHandlers in _interactionHandlers.Values)
        {
            CallbackReturn callbackReturn = HandleInteraction(interactionHandlers, hits, context);

            if (callbackReturn == CallbackReturn.Lock)
            {
                context.BlockingHandlers = interactionHandlers;
                break;
            }
            if (callbackReturn == CallbackReturn.Stop)
            {
                break;
            }
        }
        if (context.ActionType == ActionType.Up)
        {
            _contextsByFingerId.Remove(context.InputData.Touch.fingerId);
            ReleaseCleanup?.Invoke();
        }
    }

    /// <summary>
    /// Handles interaction with the blocking handler, and removes it if the _interactionContext.ActionType is Up
    /// </summary>
    /// <param name="hits"></param>
    /// <param name="context"></param>
    private void InvokeBlockingHandlers(List<GameObject> hits, InteractionContext context)
    {
        HandleInteraction(context.BlockingHandlers, hits, context, true);

        if (context.ActionType == ActionType.Up)
        {
            _contextsByFingerId.Remove(context.InputData.Touch.fingerId);
            ReleaseCleanup?.Invoke();
        }
    }

    /// <summary>
    /// Calls the handlers depending on the situation
    /// </summary>
    /// <param name="interactionHandlers"></param>
    /// <param name="hits"></param>
    /// <param name="context"></param>
    /// <param name="isBlocking"></param>
    /// <returns></returns>
    private CallbackReturn HandleInteraction(InteractionHandlers interactionHandlers, List<GameObject> hits, InteractionContext context, bool isBlocking = false)
    {
        var validHits = interactionHandlers.Tag == default ? hits : hits.Where(hit => hit.CompareTag(interactionHandlers.Tag)).ToList();

        foreach (GameObject hit in validHits)
        {
            InteractiveElement interactiveElement = hit.GetComponent<InteractiveElement>();

            return HandleClickOnInteractiveElement(interactionHandlers, context, interactiveElement);
        }
        if (interactionHandlers.Tag == default || (isBlocking && !validHits.Any()))
        {
            return HandleClickOnInteractiveElement(interactionHandlers, context, default);
        }
        return CallbackReturn.Continue;
    }

    /// <summary>
    /// Registers clicked element, and validates the click if it's released over the same element
    /// Also calls pressed and released handlers when necessary
    /// </summary>
    private CallbackReturn HandleClickOnInteractiveElement(InteractionHandlers interactionHandlers, InteractionContext context, InteractiveElement newElement)
    {
        switch (context.ActionType)
        {
            case ActionType.Down:
            {
                // Register clicked element and calls pressed handler
                context.FirstInteractiveElement = newElement;
                context.CurrentInteractiveElement = newElement;
                var shouldLock = interactionHandlers.PressHandler?.Invoke(context);
                return shouldLock != default && (bool)shouldLock ? CallbackReturn.Lock : CallbackReturn.Continue;
            }
            case ActionType.Held:
            {
                // If current element changed, update context and notify handlers
                if (context.CurrentInteractiveElement != newElement)
                {
                    context.CurrentInteractiveElement = newElement;
                    interactionHandlers.ChangedHandler?.Invoke(context);
                }
                // Calls hold handler
                var shouldLock = interactionHandlers.HoldHandler?.Invoke(context);
                return shouldLock != default && (bool)shouldLock ? CallbackReturn.Lock : CallbackReturn.Continue;
            }
            case ActionType.Up:
            {
                // Invokes release and click validated handlers
                context.CurrentInteractiveElement = newElement;
                var shouldStop = interactionHandlers.ReleaseHandler?.Invoke(context);
                if (context.FirstInteractiveElement == context.CurrentInteractiveElement)
                {
                    interactionHandlers.ClickHandler?.Invoke(context);
                }
                return shouldStop != default && (bool)shouldStop ? CallbackReturn.Stop : CallbackReturn.Continue;
            }
        }
        return CallbackReturn.Stop;
    }

    /// <summary>
    /// Gets the ActionType enum value from Unity's methods
    /// </summary>
    /// <param name="fingerId"></param>
    /// <returns></returns>
    private ActionType GetActionType(int fingerId)
    {
        switch (BetterTouches.I.TouchesByFingerId[fingerId].phase)
        {
            case TouchPhase.Began:
                return ActionType.Down;
            case TouchPhase.Ended:
                return ActionType.Up;
            default:
                return ActionType.Held;
        }
    }

    private bool IsCursorOnSprite(SpriteRenderer spriteRenderer, Vector2 cursorPosition)
    {
        Sprite sprite = spriteRenderer.sprite;
        Vector2 position = spriteRenderer.transform.position;
        var relativeCursorPosition = (Vector2)Camera.main.ScreenToWorldPoint(cursorPosition) - position;

        int x = Mathf.FloorToInt(sprite.pixelsPerUnit * relativeCursorPosition.x / spriteRenderer.transform.localScale.x) + (int)sprite.pivot.x;
        int y = Mathf.FloorToInt(sprite.pixelsPerUnit * relativeCursorPosition.y / spriteRenderer.transform.localScale.y) + (int)sprite.pivot.y;

        return (x > 0 && x <= sprite.rect.size.x)
            && (y > 0 && y <= sprite.rect.size.y)
            && (sprite.texture.GetPixel(x, y).a >= 0.1f);
    }
}